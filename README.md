# MyColoc™ ~ Mouaz MOHAMED
MyColoc™ est une application de bureau Windows développée en C# par Mouaz et Sofiane via le framework Microsoft .NET et l'interface graphique Windows Forms.<br>
C'est une application du CROUS pour une colocation permettant de gérer les colocataires et les dépenses.

---

## Diagramme des cas d'utilisation (User Case Diagram) de notre application
```plantuml
left to right direction
:Utilisateur: as Utilisateur
package <uc>MyColoc™{
    Utilisateur --> (Gérer les colocataires)
    Utilisateur --> (Gérer les dépenses)
    (Gérer les colocataires) ..> (Consulter la liste des colocataires)
    (Gérer les colocataires) ..> (Ajouter un colocataire)
    (Gérer les colocataires) ..> (Modifier un colocataire)
    (Gérer les colocataires) ..> (Supprimer un colocataire)
    (Gérer les dépenses) ..> (Consulter la liste des dépenses)
    (Gérer les dépenses) ..> (Ajouter une dépense)
    (Gérer les dépenses) ..> (Modifier une dépense)
    (Gérer les dépenses) ..> (Supprimer une dépense)
}
```

---

## Base de données
```plantuml
class Colocataire{
    id: INT(11)
    prenom: VARCHAR(50)
    nom: VARCHAR(50)
    mail: VARCHAR(50)
    telephone: VARCHAR(20)
    PRIMARY KEY(id)
}

class Dépense{
    id: INT(11)
    dateDepense: DATETIME
    texte: VARCHAR(50)
    justificatif: VARCHAR(256)
    montant: NUMERIC(10,2)
    idColocataire: INT(11)
    PRIMARY KEY(id)
    FOREIGN KEY(idColocataire) REFERCENCES colocataire(id)
}
```

```sql
DROP DATABASE IF EXISTS coloc;
CREATE DATABASE coloc DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
use coloc;

create table colocataire(
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    prenom varchar(50),
    nom varchar(50),
    mail varchar(50),
    telephone varchar(20),
    PRIMARY KEY (id)
);

create table depense(
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    dateDepense DateTime,
    text varchar(50),
    justificatif varchar(256),
    montant numeric(10,2),
    idColocataire int(11) UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idColocataire) REFERENCES colocataire(id)
);
```

---


## Accueil de notre application
Voici l'écran d'accueil de notre application :<br>
![accueil](Annexes/accueil.png)<br><br>
Nous pouvons gérer les colocataires et les dépenses.<br><br>

---

## Gérer les colocataires
Voici l'écran de la liste des colocataires :<br>
![coloc1](Annexes/coloc1.png)<br><br>
Nous pouvons ajouter un colocataire :<br>
![coloc2](Annexes/coloc2.png)<br><br>
Nous pouvons modifier un colocataire :<br>
![coloc3](Annexes/coloc3.png)<br><br>
Nous pouvons supprimer un colocataire :<br>
![coloc4](Annexes/coloc4.png)<br><br>

---

## Gérer les dépenses
Voici l'écran de la liste des dépenses :<br>
![depense1](Annexes/depense1.png)<br><br>
Nous pouvons ajouter une dépense :<br>
![depense2](Annexes/depense2.png)<br><br>
Nous pouvons modifier une dépense :<br>
![depense3](Annexes/depense3.png)<br><br>
Nous pouvons supprimer une dépense :<br>
![depense4](Annexes/depense4.png)<br><br>