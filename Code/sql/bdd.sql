drop database coloc;

create database coloc CHARACTER SET utf8mb4;

use coloc;


create table colocataire(
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    prenom varchar(50),
    nom varchar(50),
    mail varchar(50),
    telephone varchar(20),
    PRIMARY KEY (id)
)CHARACTER SET utf8mb4;

create table depense(
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    dateDepense DateTime,
    text varchar(50),
    justificatif varchar(256),
    montant numeric(10,2),
    reparti boolean,
    idColocataire int(11) UNSIGNED NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idColocataire) REFERENCES colocataire(id)
)CHARACTER SET utf8mb4;


