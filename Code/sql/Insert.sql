insert into colocataire(prenom,nom, mail, telephone) values("Mouaz","MOHAMED","mouaz@mycoloc.fr","06 01 02 03 04");
insert into colocataire(prenom,nom, mail, telephone) values("Sofiane","BOUHOURIA","sofiane@mycoloc.fr","07 01 02 03 04");

insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2024-01-08", "Achat iPhone 15 Pro Max - 1To", "iPhone.png", 1979.00, FALSE, 1);
insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values("2023-05-30", "Achat Mac Studio ", "MacStudio.png", 15257.98, FALSE, 2);