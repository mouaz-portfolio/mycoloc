﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FAjoutDepense : Form
    {
        private FDepense f;
        public FAjoutDepense(FDepense f)
        {
            InitializeComponent();
            this.f = f;
            DaoColoc daoColcs = new DaoColoc();
            this.LoadListe(daoColcs.GetAll());
            cbColocataire.SelectedIndex = 0;
            btnAjouterDepense.Click += BtnAjouterDepense_Click;
            btnRetourDepense.Click += BtnRetourDepense_Click;
            this.FormClosing += FAjoutDepense_FormClosing;
            this.btnOpen.Click += BtnOpen_Click;
            od_ouvrir.FileOk += Od_ouvrir_FileOk;
        }

        private void Od_ouvrir_FileOk(object sender, CancelEventArgs e)
        {
            //on rentre dans la methode quand le bouton ouvrir de la boite de dialogue est cliqué
            tbAjoutJustificatif.Text = od_ouvrir.FileName;
        }

        private void BtnOpen_Click(object sender, EventArgs e)
        {
            //on rentre dans la méthode quand on clique sur le bouton ouvrir du menu
            od_ouvrir.ShowDialog(); //quand le bouton ouvrir est cliqué, la boite de dialogue est ouverte
        }

        private void FAjoutDepense_FormClosing(object sender, FormClosingEventArgs e)
        {
            FDepense f = new FDepense();
            f.Show();
        }

        private void BtnRetourDepense_Click(object sender, EventArgs e)
        {
            this.Hide();
            FDepense f = new FDepense();
            f.Show();
        }

        private void BtnAjouterDepense_Click(object sender, EventArgs e)
        {
            DaoDepense daoDepense = new DaoDepense();
            if (cbColocataire.SelectedIndex != -1 && tbAjoutDesignation.Text != "" && tbAjoutJustificatif.Text != "" && tbAjoutMontant.Text != "")
            {
                try
                {
                    int position = cbColocataire.SelectedIndex;
                    Colocataire unColoc = (Colocataire)cbColocataire.Items[position];
                    decimal montant = Convert.ToDecimal(tbAjoutMontant.Text);
                    Depense uneDepense = new Depense(dtpAjoutDate.Value, tbAjoutDesignation.Text, tbAjoutJustificatif.Text, Convert.ToDecimal(tbAjoutMontant.Text), false, unColoc.IdColoc);
                    daoDepense.Insert(uneDepense);
                    tbAjoutDesignation.Text = "";
                    tbAjoutJustificatif.Text = "";
                    tbAjoutMontant.Text = "";
                    f.Clear();
                    f.LoadListe(daoDepense.GetAllNonReparti());
                    this.Hide();
                    f.Show();
                }
                catch
                {
                    MessageBox.Show("Merci de bien vouloir renseigner un décimal pour le montant !");
                }
            }
            else
            {
                MessageBox.Show("Ne pas laisser des champs vides !");
            }
        }

        private void LoadListe(List<Colocataire> listeColoc)
        {
            foreach (Colocataire c in listeColoc)
            {
                cbColocataire.Items.Add(c);
            }
        }
    }
}
