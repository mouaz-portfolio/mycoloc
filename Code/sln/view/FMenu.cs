﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;

namespace Coloc.View {
    public partial class FMenu:Form {
        public FMenu() {
            InitializeComponent();
            btnGererDepenses.Click += BtnGererDepenses_Click;
            btnGererColocataires.Click += BtnGererColocataires_Click;
            DaoConnection.SetStringConnection("root", "siojjr", "localhost", "coloc");
        }


        private void BtnGererColocataires_Click(object sender, EventArgs e)
        {
            FColocataires f = new FColocataires();
            f.Show();
            this.Hide();
        }

        private void BtnGererDepenses_Click(object sender, EventArgs e)
        {
            FDepense f = new FDepense();
            f.Show();
            this.Hide();
        }
    }
}
