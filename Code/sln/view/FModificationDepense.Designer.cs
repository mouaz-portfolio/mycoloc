﻿
namespace Coloc.View
{
    partial class FModificationDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FModificationDepense));
            this.labelModifDepense = new System.Windows.Forms.Label();
            this.btnModificationDepense = new System.Windows.Forms.Button();
            this.tbModifMontant = new System.Windows.Forms.TextBox();
            this.tbModifJustificatif = new System.Windows.Forms.TextBox();
            this.tbModifDesignation = new System.Windows.Forms.TextBox();
            this.labelModifJustificatif = new System.Windows.Forms.Label();
            this.labelModifDesignation = new System.Windows.Forms.Label();
            this.labelModifMontant = new System.Windows.Forms.Label();
            this.labelModifDateDepense = new System.Windows.Forms.Label();
            this.dtpModifDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cbColocataire = new System.Windows.Forms.ComboBox();
            this.btnRetourDepense = new System.Windows.Forms.Button();
            this.od_ouvrir = new System.Windows.Forms.OpenFileDialog();
            this.btnOpen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelModifDepense
            // 
            this.labelModifDepense.AutoSize = true;
            this.labelModifDepense.BackColor = System.Drawing.Color.RoyalBlue;
            this.labelModifDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDepense.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelModifDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDepense.Location = new System.Drawing.Point(512, 41);
            this.labelModifDepense.Name = "labelModifDepense";
            this.labelModifDepense.Size = new System.Drawing.Size(299, 32);
            this.labelModifDepense.TabIndex = 6;
            this.labelModifDepense.Text = "Modifier une dépense";
            // 
            // btnModificationDepense
            // 
            this.btnModificationDepense.BackColor = System.Drawing.Color.Snow;
            this.btnModificationDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificationDepense.FlatAppearance.BorderSize = 0;
            this.btnModificationDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificationDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnModificationDepense.Location = new System.Drawing.Point(340, 344);
            this.btnModificationDepense.Name = "btnModificationDepense";
            this.btnModificationDepense.Size = new System.Drawing.Size(178, 30);
            this.btnModificationDepense.TabIndex = 33;
            this.btnModificationDepense.Text = "Modifer la dépense";
            this.btnModificationDepense.UseVisualStyleBackColor = false;
            // 
            // tbModifMontant
            // 
            this.tbModifMontant.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbModifMontant.Location = new System.Drawing.Point(352, 281);
            this.tbModifMontant.Name = "tbModifMontant";
            this.tbModifMontant.Size = new System.Drawing.Size(281, 27);
            this.tbModifMontant.TabIndex = 30;
            // 
            // tbModifJustificatif
            // 
            this.tbModifJustificatif.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbModifJustificatif.Location = new System.Drawing.Point(352, 232);
            this.tbModifJustificatif.Margin = new System.Windows.Forms.Padding(0);
            this.tbModifJustificatif.Name = "tbModifJustificatif";
            this.tbModifJustificatif.Size = new System.Drawing.Size(281, 27);
            this.tbModifJustificatif.TabIndex = 29;
            // 
            // tbModifDesignation
            // 
            this.tbModifDesignation.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbModifDesignation.Location = new System.Drawing.Point(352, 187);
            this.tbModifDesignation.Name = "tbModifDesignation";
            this.tbModifDesignation.Size = new System.Drawing.Size(281, 27);
            this.tbModifDesignation.TabIndex = 28;
            // 
            // labelModifJustificatif
            // 
            this.labelModifJustificatif.AutoSize = true;
            this.labelModifJustificatif.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifJustificatif.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelModifJustificatif.ForeColor = System.Drawing.Color.Snow;
            this.labelModifJustificatif.Location = new System.Drawing.Point(235, 235);
            this.labelModifJustificatif.Name = "labelModifJustificatif";
            this.labelModifJustificatif.Size = new System.Drawing.Size(90, 19);
            this.labelModifJustificatif.TabIndex = 26;
            this.labelModifJustificatif.Text = "Justificatif :";
            // 
            // labelModifDesignation
            // 
            this.labelModifDesignation.AutoSize = true;
            this.labelModifDesignation.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDesignation.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelModifDesignation.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDesignation.Location = new System.Drawing.Point(217, 190);
            this.labelModifDesignation.Name = "labelModifDesignation";
            this.labelModifDesignation.Size = new System.Drawing.Size(108, 19);
            this.labelModifDesignation.TabIndex = 25;
            this.labelModifDesignation.Text = "Désignation :";
            // 
            // labelModifMontant
            // 
            this.labelModifMontant.AutoSize = true;
            this.labelModifMontant.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifMontant.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelModifMontant.ForeColor = System.Drawing.Color.Snow;
            this.labelModifMontant.Location = new System.Drawing.Point(245, 284);
            this.labelModifMontant.Name = "labelModifMontant";
            this.labelModifMontant.Size = new System.Drawing.Size(80, 19);
            this.labelModifMontant.TabIndex = 24;
            this.labelModifMontant.Text = "Montant :";
            // 
            // labelModifDateDepense
            // 
            this.labelModifDateDepense.AutoSize = true;
            this.labelModifDateDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelModifDateDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelModifDateDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelModifDateDepense.Location = new System.Drawing.Point(156, 149);
            this.labelModifDateDepense.Name = "labelModifDateDepense";
            this.labelModifDateDepense.Size = new System.Drawing.Size(169, 19);
            this.labelModifDateDepense.TabIndex = 23;
            this.labelModifDateDepense.Text = "Date de la dépense :";
            // 
            // dtpModifDate
            // 
            this.dtpModifDate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.dtpModifDate.Location = new System.Drawing.Point(352, 143);
            this.dtpModifDate.Name = "dtpModifDate";
            this.dtpModifDate.Size = new System.Drawing.Size(281, 27);
            this.dtpModifDate.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(96, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 19);
            this.label1.TabIndex = 35;
            this.label1.Text = "Sélectionner un colocataire :";
            // 
            // cbColocataire
            // 
            this.cbColocataire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbColocataire.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.cbColocataire.FormattingEnabled = true;
            this.cbColocataire.Location = new System.Drawing.Point(352, 105);
            this.cbColocataire.Name = "cbColocataire";
            this.cbColocataire.Size = new System.Drawing.Size(805, 27);
            this.cbColocataire.TabIndex = 34;
            // 
            // btnRetourDepense
            // 
            this.btnRetourDepense.BackColor = System.Drawing.Color.Snow;
            this.btnRetourDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourDepense.FlatAppearance.BorderSize = 0;
            this.btnRetourDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnRetourDepense.Location = new System.Drawing.Point(12, 402);
            this.btnRetourDepense.Name = "btnRetourDepense";
            this.btnRetourDepense.Size = new System.Drawing.Size(71, 36);
            this.btnRetourDepense.TabIndex = 37;
            this.btnRetourDepense.Text = "Retour";
            this.btnRetourDepense.UseVisualStyleBackColor = false;
            // 
            // od_ouvrir
            // 
            this.od_ouvrir.FileName = "openFileDialog1";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.Snow;
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatAppearance.BorderSize = 0;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnOpen.Location = new System.Drawing.Point(645, 225);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(194, 39);
            this.btnOpen.TabIndex = 38;
            this.btnOpen.Text = "Sélectionner un fichier";
            this.btnOpen.UseVisualStyleBackColor = false;
            // 
            // FModificationDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1238, 450);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnRetourDepense);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbColocataire);
            this.Controls.Add(this.btnModificationDepense);
            this.Controls.Add(this.tbModifMontant);
            this.Controls.Add(this.tbModifJustificatif);
            this.Controls.Add(this.tbModifDesignation);
            this.Controls.Add(this.labelModifJustificatif);
            this.Controls.Add(this.labelModifDesignation);
            this.Controls.Add(this.labelModifMontant);
            this.Controls.Add(this.labelModifDateDepense);
            this.Controls.Add(this.dtpModifDate);
            this.Controls.Add(this.labelModifDepense);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FModificationDepense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier une dépense - MyColoc™";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelModifDepense;
        private System.Windows.Forms.Button btnModificationDepense;
        private System.Windows.Forms.TextBox tbModifMontant;
        private System.Windows.Forms.TextBox tbModifJustificatif;
        private System.Windows.Forms.TextBox tbModifDesignation;
        private System.Windows.Forms.Label labelModifJustificatif;
        private System.Windows.Forms.Label labelModifDesignation;
        private System.Windows.Forms.Label labelModifMontant;
        private System.Windows.Forms.Label labelModifDateDepense;
        private System.Windows.Forms.DateTimePicker dtpModifDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbColocataire;
        private System.Windows.Forms.Button btnRetourDepense;
        private System.Windows.Forms.OpenFileDialog od_ouvrir;
        private System.Windows.Forms.Button btnOpen;
    }
}