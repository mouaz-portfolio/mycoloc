﻿
namespace Coloc.View
{
    partial class FAjoutColoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAjoutColoc));
            this.btnAjouterColoc = new System.Windows.Forms.Button();
            this.tbAjoutTel = new System.Windows.Forms.TextBox();
            this.tbAjoutMail = new System.Windows.Forms.TextBox();
            this.tbAjoutNom = new System.Windows.Forms.TextBox();
            this.labelMail = new System.Windows.Forms.Label();
            this.labelNom = new System.Windows.Forms.Label();
            this.labelTel = new System.Windows.Forms.Label();
            this.labelAjouterColoc = new System.Windows.Forms.Label();
            this.tbAjoutPrenom = new System.Windows.Forms.TextBox();
            this.labelPrenom = new System.Windows.Forms.Label();
            this.btnRetourColoc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAjouterColoc
            // 
            this.btnAjouterColoc.BackColor = System.Drawing.Color.Snow;
            this.btnAjouterColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAjouterColoc.FlatAppearance.BorderSize = 0;
            this.btnAjouterColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnAjouterColoc.Location = new System.Drawing.Point(332, 339);
            this.btnAjouterColoc.Name = "btnAjouterColoc";
            this.btnAjouterColoc.Size = new System.Drawing.Size(81, 30);
            this.btnAjouterColoc.TabIndex = 35;
            this.btnAjouterColoc.Text = "Ajouter";
            this.btnAjouterColoc.UseVisualStyleBackColor = false;
            // 
            // tbAjoutTel
            // 
            this.tbAjoutTel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbAjoutTel.Location = new System.Drawing.Point(298, 276);
            this.tbAjoutTel.Name = "tbAjoutTel";
            this.tbAjoutTel.Size = new System.Drawing.Size(281, 27);
            this.tbAjoutTel.TabIndex = 32;
            // 
            // tbAjoutMail
            // 
            this.tbAjoutMail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbAjoutMail.Location = new System.Drawing.Point(298, 226);
            this.tbAjoutMail.Name = "tbAjoutMail";
            this.tbAjoutMail.Size = new System.Drawing.Size(281, 27);
            this.tbAjoutMail.TabIndex = 31;
            // 
            // tbAjoutNom
            // 
            this.tbAjoutNom.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbAjoutNom.Location = new System.Drawing.Point(298, 175);
            this.tbAjoutNom.Name = "tbAjoutNom";
            this.tbAjoutNom.Size = new System.Drawing.Size(281, 27);
            this.tbAjoutNom.TabIndex = 30;
            // 
            // labelMail
            // 
            this.labelMail.AutoSize = true;
            this.labelMail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelMail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelMail.ForeColor = System.Drawing.Color.Snow;
            this.labelMail.Location = new System.Drawing.Point(167, 229);
            this.labelMail.Name = "labelMail";
            this.labelMail.Size = new System.Drawing.Size(50, 19);
            this.labelMail.TabIndex = 28;
            this.labelMail.Text = "Mail :";
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelNom.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelNom.ForeColor = System.Drawing.Color.Snow;
            this.labelNom.Location = new System.Drawing.Point(162, 178);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(55, 19);
            this.labelNom.TabIndex = 27;
            this.labelNom.Text = "Nom :";
            // 
            // labelTel
            // 
            this.labelTel.AutoSize = true;
            this.labelTel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelTel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.labelTel.ForeColor = System.Drawing.Color.Snow;
            this.labelTel.Location = new System.Drawing.Point(118, 279);
            this.labelTel.Name = "labelTel";
            this.labelTel.Size = new System.Drawing.Size(99, 19);
            this.labelTel.TabIndex = 26;
            this.labelTel.Text = "Téléphone :";
            // 
            // labelAjouterColoc
            // 
            this.labelAjouterColoc.AutoSize = true;
            this.labelAjouterColoc.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelAjouterColoc.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAjouterColoc.ForeColor = System.Drawing.Color.Snow;
            this.labelAjouterColoc.Location = new System.Drawing.Point(216, 47);
            this.labelAjouterColoc.Name = "labelAjouterColoc";
            this.labelAjouterColoc.Size = new System.Drawing.Size(306, 32);
            this.labelAjouterColoc.TabIndex = 22;
            this.labelAjouterColoc.Text = "Ajouter un colocataire";
            // 
            // tbAjoutPrenom
            // 
            this.tbAjoutPrenom.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.tbAjoutPrenom.Location = new System.Drawing.Point(298, 125);
            this.tbAjoutPrenom.Name = "tbAjoutPrenom";
            this.tbAjoutPrenom.Size = new System.Drawing.Size(281, 27);
            this.tbAjoutPrenom.TabIndex = 37;
            // 
            // labelPrenom
            // 
            this.labelPrenom.AutoSize = true;
            this.labelPrenom.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelPrenom.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrenom.ForeColor = System.Drawing.Color.Snow;
            this.labelPrenom.Location = new System.Drawing.Point(140, 128);
            this.labelPrenom.Name = "labelPrenom";
            this.labelPrenom.Size = new System.Drawing.Size(77, 19);
            this.labelPrenom.TabIndex = 36;
            this.labelPrenom.Text = "Prénom :";
            // 
            // btnRetourColoc
            // 
            this.btnRetourColoc.BackColor = System.Drawing.Color.Snow;
            this.btnRetourColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourColoc.FlatAppearance.BorderSize = 0;
            this.btnRetourColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnRetourColoc.Location = new System.Drawing.Point(12, 339);
            this.btnRetourColoc.Name = "btnRetourColoc";
            this.btnRetourColoc.Size = new System.Drawing.Size(72, 44);
            this.btnRetourColoc.TabIndex = 38;
            this.btnRetourColoc.Text = "Retour";
            this.btnRetourColoc.UseVisualStyleBackColor = false;
            // 
            // FAjoutColoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(724, 394);
            this.Controls.Add(this.btnRetourColoc);
            this.Controls.Add(this.tbAjoutPrenom);
            this.Controls.Add(this.labelPrenom);
            this.Controls.Add(this.btnAjouterColoc);
            this.Controls.Add(this.tbAjoutTel);
            this.Controls.Add(this.tbAjoutMail);
            this.Controls.Add(this.tbAjoutNom);
            this.Controls.Add(this.labelMail);
            this.Controls.Add(this.labelNom);
            this.Controls.Add(this.labelTel);
            this.Controls.Add(this.labelAjouterColoc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FAjoutColoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un colocataire - MyColoc™";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAjouterColoc;
        private System.Windows.Forms.TextBox tbAjoutTel;
        private System.Windows.Forms.TextBox tbAjoutMail;
        private System.Windows.Forms.TextBox tbAjoutNom;
        private System.Windows.Forms.Label labelMail;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelTel;
        private System.Windows.Forms.Label labelAjouterColoc;
        private System.Windows.Forms.TextBox tbAjoutPrenom;
        private System.Windows.Forms.Label labelPrenom;
        private System.Windows.Forms.Button btnRetourColoc;
    }
}