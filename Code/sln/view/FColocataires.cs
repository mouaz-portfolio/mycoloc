﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Model;
using Coloc.Dao;

namespace Coloc.View
{
    public partial class FColocataires : Form
    {
        public FColocataires()
        {
            InitializeComponent();
            btnRetourColoc.Click += BtnRetourColoc_Click;
            DaoColoc daoColoc = new DaoColoc();
            this.LoadListe(daoColoc.GetAll());
            btnSaisirColoc.Click += BtnSaisirColoc_Click;
            btnModifColoc.Click += BtnModifColoc_Click;
            btnSupprimerColoc.Click += BtnSupprimerColoc_Click;
            this.FormClosing += FColocataires_FormClosing;
        }

        private void FColocataires_FormClosing(object sender, FormClosingEventArgs e)
        {
            FMenu f = new FMenu();
            f.Show();
        }

        private void BtnSupprimerColoc_Click(object sender, EventArgs e)
        {
            if (lbColocataire.SelectedIndex == -1)
            {
                MessageBox.Show("Selectionner un colocataire à supprimer !");
            }
            else
            {
                DaoColoc daoColoc = new DaoColoc();
                int position = lbColocataire.SelectedIndex;
                Colocataire unColoc = (Colocataire)lbColocataire.Items[position];
                int id = unColoc.IdColoc;
                try
                {
                    daoColoc.Delete(unColoc);
                    lbColocataire.Items.RemoveAt(position);
                }
                catch
                {
                    var result = MessageBox.Show("Veux-tu vraiment supprimer le colocataire ? Cela va entraîner la suppression de toute ses dépense !!!", "⚠️ Warning ⚠️",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        DaoDepense daoDenpense = new DaoDepense();
                        daoDenpense.Delete(id);
                        daoColoc.Delete(unColoc);
                        lbColocataire.Items.RemoveAt(position);
                    }
                    else
                    {
                        return;
                    }
                }  
            }
        }

        private void BtnRetourColoc_Click(object sender, EventArgs e)
        {
            this.Hide();
            FMenu f = new FMenu();
            f.Show();
        }

        private void BtnModifColoc_Click(object sender, EventArgs e)
        {
            if(lbColocataire.SelectedIndex == -1){
                MessageBox.Show("Selectionner un colocataire à modifier !");
            }
            else
            {
                int position = lbColocataire.SelectedIndex;
                Colocataire c = (Colocataire)lbColocataire.Items[position];
                FModificationColoc f = new FModificationColoc(this,c.IdColoc, c.Nom, c.Prenom, c.Mail, c.Telephone);
                f.Show();
                this.Hide();
            }
        }

        private void BtnSaisirColoc_Click(object sender, EventArgs e)
        {
            FAjoutColoc f = new FAjoutColoc(this);
            f.Show();
            this.Hide();
        }

        public void LoadListe(List<Colocataire> listeColoc)
        {
            foreach(Colocataire c in listeColoc)
            {
                lbColocataire.Items.Add(c);
            }
        }

        public void Clear()
        {
            lbColocataire.Items.Clear();
        }
    }
}
