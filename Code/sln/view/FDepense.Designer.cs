﻿
namespace Coloc.View
{
    partial class FDepense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FDepense));
            this.labelConsultationDepense = new System.Windows.Forms.Label();
            this.lbDepense = new System.Windows.Forms.ListBox();
            this.btnRetourDepense = new System.Windows.Forms.Button();
            this.btnSuppDepense = new System.Windows.Forms.Button();
            this.btnModifDepense = new System.Windows.Forms.Button();
            this.btnSaisirDepense = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelConsultationDepense
            // 
            this.labelConsultationDepense.AutoSize = true;
            this.labelConsultationDepense.BackColor = System.Drawing.Color.RoyalBlue;
            this.labelConsultationDepense.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelConsultationDepense.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelConsultationDepense.ForeColor = System.Drawing.Color.Snow;
            this.labelConsultationDepense.Location = new System.Drawing.Point(424, 31);
            this.labelConsultationDepense.Name = "labelConsultationDepense";
            this.labelConsultationDepense.Size = new System.Drawing.Size(365, 32);
            this.labelConsultationDepense.TabIndex = 5;
            this.labelConsultationDepense.Text = "Consultation des dépenses";
            // 
            // lbDepense
            // 
            this.lbDepense.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDepense.FormattingEnabled = true;
            this.lbDepense.ItemHeight = 16;
            this.lbDepense.Location = new System.Drawing.Point(177, 96);
            this.lbDepense.Name = "lbDepense";
            this.lbDepense.Size = new System.Drawing.Size(1030, 324);
            this.lbDepense.TabIndex = 6;
            // 
            // btnRetourDepense
            // 
            this.btnRetourDepense.BackColor = System.Drawing.Color.Snow;
            this.btnRetourDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourDepense.FlatAppearance.BorderSize = 0;
            this.btnRetourDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnRetourDepense.Location = new System.Drawing.Point(29, 363);
            this.btnRetourDepense.Name = "btnRetourDepense";
            this.btnRetourDepense.Size = new System.Drawing.Size(117, 60);
            this.btnRetourDepense.TabIndex = 7;
            this.btnRetourDepense.Text = "Retour";
            this.btnRetourDepense.UseVisualStyleBackColor = false;
            // 
            // btnSuppDepense
            // 
            this.btnSuppDepense.BackColor = System.Drawing.Color.Snow;
            this.btnSuppDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSuppDepense.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnSuppDepense.FlatAppearance.BorderSize = 0;
            this.btnSuppDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuppDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnSuppDepense.Location = new System.Drawing.Point(12, 273);
            this.btnSuppDepense.Name = "btnSuppDepense";
            this.btnSuppDepense.Size = new System.Drawing.Size(151, 46);
            this.btnSuppDepense.TabIndex = 12;
            this.btnSuppDepense.Text = "Supprimer ";
            this.btnSuppDepense.UseVisualStyleBackColor = false;
            // 
            // btnModifDepense
            // 
            this.btnModifDepense.BackColor = System.Drawing.Color.Snow;
            this.btnModifDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModifDepense.FlatAppearance.BorderSize = 0;
            this.btnModifDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnModifDepense.Location = new System.Drawing.Point(12, 190);
            this.btnModifDepense.Name = "btnModifDepense";
            this.btnModifDepense.Size = new System.Drawing.Size(151, 46);
            this.btnModifDepense.TabIndex = 11;
            this.btnModifDepense.Text = "Modifier";
            this.btnModifDepense.UseVisualStyleBackColor = false;
            // 
            // btnSaisirDepense
            // 
            this.btnSaisirDepense.BackColor = System.Drawing.Color.Snow;
            this.btnSaisirDepense.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaisirDepense.FlatAppearance.BorderSize = 0;
            this.btnSaisirDepense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaisirDepense.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnSaisirDepense.Location = new System.Drawing.Point(12, 96);
            this.btnSaisirDepense.Name = "btnSaisirDepense";
            this.btnSaisirDepense.Size = new System.Drawing.Size(151, 46);
            this.btnSaisirDepense.TabIndex = 10;
            this.btnSaisirDepense.Text = "Ajouter";
            this.btnSaisirDepense.UseVisualStyleBackColor = false;
            // 
            // FDepense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1219, 450);
            this.Controls.Add(this.btnSuppDepense);
            this.Controls.Add(this.btnModifDepense);
            this.Controls.Add(this.btnSaisirDepense);
            this.Controls.Add(this.btnRetourDepense);
            this.Controls.Add(this.lbDepense);
            this.Controls.Add(this.labelConsultationDepense);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FDepense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultation des dépenses - MyColoc™";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConsultationDepense;
        private System.Windows.Forms.ListBox lbDepense;
        private System.Windows.Forms.Button btnRetourDepense;
        private System.Windows.Forms.Button btnSuppDepense;
        private System.Windows.Forms.Button btnModifDepense;
        private System.Windows.Forms.Button btnSaisirDepense;
    }
}