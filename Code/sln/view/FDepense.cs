﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Model;
using Coloc.Dao;

namespace Coloc.View
{
    public partial class FDepense : Form
    {
        public FDepense()
        {
            InitializeComponent();
            btnRetourDepense.Click += BtnRetourDepense_Click;
            DaoDepense daoDepense = new DaoDepense();
            this.LoadListe(daoDepense.GetAllNonReparti());
            btnSaisirDepense.Click += BtnSaisirDepense_Click;
            btnModifDepense.Click += BtnModifDepense_Click;
            btnSuppDepense.Click += BtnSuppDepense_Click;
            this.FormClosing += FDepense_FormClosing;
        }

        private void FDepense_FormClosing(object sender, FormClosingEventArgs e)
        {
            FMenu f = new FMenu();
            f.Show();
        }

        private void BtnRetourDepense_Click(object sender, EventArgs e)
        {
            this.Hide();
            FMenu f = new FMenu();
            f.Show();
        }

        public void LoadListe(List<Depense> listeDepenses)
        {
            DaoColoc daoColoc = new DaoColoc();
            foreach (Depense d in listeDepenses)
            {
                d.SetListColoc(daoColoc.GetAll());
                lbDepense.Items.Add(d);
            }
        }

        public void Clear()
        {
            lbDepense.Items.Clear();
        }

        private void BtnSuppDepense_Click(object sender, EventArgs e)
        {
            if (lbDepense.SelectedIndex == -1)
            {
                MessageBox.Show("Selectionner un colocataire à supprimer !");
            }
            else
            {
                int position = lbDepense.SelectedIndex;
                Depense uneDepense = (Depense)lbDepense.Items[position];
                DaoDepense daoDepense = new DaoDepense();
                daoDepense.Delete(uneDepense);
                lbDepense.Items.RemoveAt(position);
            }
        }

        private void BtnModifDepense_Click(object sender, EventArgs e)
        {
            if (lbDepense.SelectedIndex == -1)
            {
                MessageBox.Show("Selectionner un colocataire à modifier !");
            }
            else
            {
                int position = lbDepense.SelectedIndex;
                Depense s = (Depense)lbDepense.Items[position];
                FModificationDepense f = new FModificationDepense(this,s.IdDepense,s.DateDepense,s.Text,s.Justificatif,s.Montant,s.Reparti,s.IdColoc);
                f.Show();
                this.Hide();
            }
        }

        private void BtnSaisirDepense_Click(object sender, EventArgs e)
        {
            FAjoutDepense f = new FAjoutDepense(this);
            f.Show();
            this.Hide();
        }
    }
}
