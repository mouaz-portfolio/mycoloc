﻿
namespace Coloc.View
{
    partial class FColocataires
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FColocataires));
            this.btnRetourColoc = new System.Windows.Forms.Button();
            this.lbColocataire = new System.Windows.Forms.ListBox();
            this.labelConsultationColocs = new System.Windows.Forms.Label();
            this.btnSupprimerColoc = new System.Windows.Forms.Button();
            this.btnModifColoc = new System.Windows.Forms.Button();
            this.btnSaisirColoc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRetourColoc
            // 
            this.btnRetourColoc.BackColor = System.Drawing.Color.Snow;
            this.btnRetourColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetourColoc.FlatAppearance.BorderSize = 0;
            this.btnRetourColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetourColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnRetourColoc.Location = new System.Drawing.Point(29, 368);
            this.btnRetourColoc.Name = "btnRetourColoc";
            this.btnRetourColoc.Size = new System.Drawing.Size(117, 60);
            this.btnRetourColoc.TabIndex = 10;
            this.btnRetourColoc.Text = "Retour";
            this.btnRetourColoc.UseVisualStyleBackColor = false;
            // 
            // lbColocataire
            // 
            this.lbColocataire.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbColocataire.FormattingEnabled = true;
            this.lbColocataire.ItemHeight = 16;
            this.lbColocataire.Location = new System.Drawing.Point(179, 101);
            this.lbColocataire.Name = "lbColocataire";
            this.lbColocataire.Size = new System.Drawing.Size(1014, 324);
            this.lbColocataire.TabIndex = 9;
            // 
            // labelConsultationColocs
            // 
            this.labelConsultationColocs.AutoSize = true;
            this.labelConsultationColocs.BackColor = System.Drawing.Color.RoyalBlue;
            this.labelConsultationColocs.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelConsultationColocs.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelConsultationColocs.ForeColor = System.Drawing.Color.Snow;
            this.labelConsultationColocs.Location = new System.Drawing.Point(412, 34);
            this.labelConsultationColocs.Name = "labelConsultationColocs";
            this.labelConsultationColocs.Size = new System.Drawing.Size(400, 32);
            this.labelConsultationColocs.TabIndex = 8;
            this.labelConsultationColocs.Text = "Consultation des colocataires";
            // 
            // btnSupprimerColoc
            // 
            this.btnSupprimerColoc.BackColor = System.Drawing.Color.Snow;
            this.btnSupprimerColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSupprimerColoc.FlatAppearance.BorderSize = 0;
            this.btnSupprimerColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSupprimerColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnSupprimerColoc.Location = new System.Drawing.Point(12, 283);
            this.btnSupprimerColoc.Name = "btnSupprimerColoc";
            this.btnSupprimerColoc.Size = new System.Drawing.Size(151, 46);
            this.btnSupprimerColoc.TabIndex = 13;
            this.btnSupprimerColoc.Text = "Supprimer ";
            this.btnSupprimerColoc.UseVisualStyleBackColor = false;
            // 
            // btnModifColoc
            // 
            this.btnModifColoc.BackColor = System.Drawing.Color.Snow;
            this.btnModifColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModifColoc.FlatAppearance.BorderSize = 0;
            this.btnModifColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnModifColoc.Location = new System.Drawing.Point(12, 196);
            this.btnModifColoc.Name = "btnModifColoc";
            this.btnModifColoc.Size = new System.Drawing.Size(151, 46);
            this.btnModifColoc.TabIndex = 12;
            this.btnModifColoc.Text = "Modifier ";
            this.btnModifColoc.UseVisualStyleBackColor = false;
            // 
            // btnSaisirColoc
            // 
            this.btnSaisirColoc.BackColor = System.Drawing.Color.Snow;
            this.btnSaisirColoc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaisirColoc.FlatAppearance.BorderSize = 0;
            this.btnSaisirColoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaisirColoc.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnSaisirColoc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSaisirColoc.Location = new System.Drawing.Point(12, 101);
            this.btnSaisirColoc.Name = "btnSaisirColoc";
            this.btnSaisirColoc.Size = new System.Drawing.Size(151, 46);
            this.btnSaisirColoc.TabIndex = 11;
            this.btnSaisirColoc.Text = "Ajouter";
            this.btnSaisirColoc.UseVisualStyleBackColor = false;
            // 
            // FColocataires
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1212, 450);
            this.Controls.Add(this.btnSupprimerColoc);
            this.Controls.Add(this.btnModifColoc);
            this.Controls.Add(this.btnSaisirColoc);
            this.Controls.Add(this.btnRetourColoc);
            this.Controls.Add(this.lbColocataire);
            this.Controls.Add(this.labelConsultationColocs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FColocataires";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultation des colocataires - MyColoc™";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRetourColoc;
        private System.Windows.Forms.ListBox lbColocataire;
        private System.Windows.Forms.Label labelConsultationColocs;
        private System.Windows.Forms.Button btnSupprimerColoc;
        private System.Windows.Forms.Button btnModifColoc;
        private System.Windows.Forms.Button btnSaisirColoc;
    }
}