﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Coloc.Dao;
using Coloc.Model;

namespace Coloc.View
{
    public partial class FModificationDepense : Form
    {
        private string designation;
        private string justificatif;
        private decimal montant;
        private bool reparti;
        private DateTime date;
        private int idColoc;
        private int id;
        private FDepense f;

        public FModificationDepense(FDepense f, int id, DateTime date, string designation,string justificatif,decimal montant,bool reparti,int idColoc)
        {
            InitializeComponent();
            btnModificationDepense.Click += BtnModificationDepense_Click;
            this.date = date;
            this.designation = designation;
            this.justificatif = justificatif;
            this.montant = montant;
            this.reparti = reparti;
            this.idColoc = idColoc;
            this.id = id;
            this.f = f;
            dtpModifDate.Value = date;
            tbModifDesignation.Text = designation;
            tbModifJustificatif.Text = justificatif;
            tbModifMontant.Text = Convert.ToString(montant);
            DaoColoc daoColoc = new DaoColoc();
            this.LoadListe(daoColoc.GetAll());
            cbColocataire.SelectedIndex = 0;
            btnRetourDepense.Click += BtnRetourDepense_Click1;
            this.FormClosing += FModificationDepense_FormClosing;
            this.btnOpen.Click += BtnOpen_Click;
            od_ouvrir.FileOk += Od_ouvrir_FileOk;
        }

        private void Od_ouvrir_FileOk(object sender, CancelEventArgs e)
        {
            //on rentre dans la methode quand le bouton ouvrir de la boite de dialogue est cliqué
            tbModifJustificatif.Text = od_ouvrir.FileName;
        }

        private void BtnOpen_Click(object sender, EventArgs e)
        {
            //on rentre dans la méthode quand on clique sur le bouton ouvrir du menu
            od_ouvrir.ShowDialog(); //quand le bouton ouvrir est cliqué, la boite de dialogue est ouverte
        }


        private void FModificationDepense_FormClosing(object sender, FormClosingEventArgs e)
        {
            FDepense f = new FDepense();
            f.Show();
        }

        private void BtnRetourDepense_Click1(object sender, EventArgs e)
        {
            this.Hide();
            FDepense f = new FDepense();
            f.Show();
        }

        private void BtnModificationDepense_Click(object sender, EventArgs e)
        {
            if (cbColocataire.SelectedIndex != -1 && tbModifDesignation.Text != "" && tbModifJustificatif.Text != "" && tbModifMontant.Text != "")
            {
                try
                {
                    decimal montant = Convert.ToDecimal(tbModifMontant.Text);
                    DaoDepense daoDepense = new DaoDepense();
                    bool reparti = false;
                    int position = cbColocataire.SelectedIndex;
                    Colocataire unColoc = (Colocataire)cbColocataire.Items[position];
                    int idColoc = unColoc.IdColoc;
                    daoDepense.Update(new Depense(id, dtpModifDate.Value, tbModifDesignation.Text, tbModifJustificatif.Text, Convert.ToDecimal(tbModifMontant.Text), reparti, idColoc));
                    tbModifDesignation.Text = "";
                    tbModifJustificatif.Text = "";
                    tbModifMontant.Text = "";
                    f.Clear();
                    f.LoadListe(daoDepense.GetAllNonReparti());
                    this.Hide();
                    f.Show();
                }
                catch
                {
                    MessageBox.Show("Merci de bien vouloir renseigner un décimal pour le montant !");
                }
            }
            else
            {
                MessageBox.Show("Ne pas laisser des champs vides !");
            }
        }

        private void BtnRetourDepense_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void LoadListe(List<Colocataire> listeColoc)
        {
            foreach (Colocataire c in listeColoc)
            {
                cbColocataire.Items.Add(c);
            }
        }

    }
}
