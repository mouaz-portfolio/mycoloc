﻿namespace Coloc.View {
    partial class FMenu {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMenu));
            this.labelMenu = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGererColocataires = new System.Windows.Forms.Button();
            this.btnGererDepenses = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMenu
            // 
            this.labelMenu.AutoSize = true;
            this.labelMenu.BackColor = System.Drawing.Color.Transparent;
            this.labelMenu.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.labelMenu.Font = new System.Drawing.Font("Century Gothic", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.labelMenu.ForeColor = System.Drawing.Color.Snow;
            this.labelMenu.Location = new System.Drawing.Point(142, 107);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(516, 32);
            this.labelMenu.TabIndex = 0;
            this.labelMenu.Text = "Bienvenue sur l\'application MyColoc™";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(432, 426);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(360, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "MyColoc™ développée par Mouaz et Sofiane";
            // 
            // btnGererColocataires
            // 
            this.btnGererColocataires.BackColor = System.Drawing.Color.Snow;
            this.btnGererColocataires.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererColocataires.FlatAppearance.BorderSize = 0;
            this.btnGererColocataires.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererColocataires.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnGererColocataires.Location = new System.Drawing.Point(148, 194);
            this.btnGererColocataires.Name = "btnGererColocataires";
            this.btnGererColocataires.Size = new System.Drawing.Size(206, 79);
            this.btnGererColocataires.TabIndex = 3;
            this.btnGererColocataires.Text = "Gérer les colocataires";
            this.btnGererColocataires.UseVisualStyleBackColor = false;
            // 
            // btnGererDepenses
            // 
            this.btnGererDepenses.BackColor = System.Drawing.Color.Snow;
            this.btnGererDepenses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererDepenses.FlatAppearance.BorderSize = 0;
            this.btnGererDepenses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererDepenses.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.btnGererDepenses.Location = new System.Drawing.Point(451, 194);
            this.btnGererDepenses.Name = "btnGererDepenses";
            this.btnGererDepenses.Size = new System.Drawing.Size(207, 79);
            this.btnGererDepenses.TabIndex = 2;
            this.btnGererDepenses.Text = "Gérer ses dépenses";
            this.btnGererDepenses.UseVisualStyleBackColor = false;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(804, 454);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelMenu);
            this.Controls.Add(this.btnGererDepenses);
            this.Controls.Add(this.btnGererColocataires);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accueil - MyColoc™";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGererColocataires;
        private System.Windows.Forms.Button btnGererDepenses;
    }
}

