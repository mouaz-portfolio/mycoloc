﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace Coloc.Dao
{
    public static class DaoConnection
    {
        private static string user;
        private static string password;
        private static string server;
        private static string database;

        private static MySqlConnection mySqlConnection;

        /// <summary>
        /// Propriété (set) qui permet d'obtenir le nom d'utilisateur qui permet de se connecter à la base de donnée
        /// </summary>
        public static string User
        {
            get { return user; }
        }

        /// <summary>
        /// Propriété (set) qui permet d'obtenir le mot de passe qui permet de se connecter à la base de donnée
        /// </summary>
        public static string Password
        {
            get { return password; }
        }

        /// <summary>
        /// Propriété (set) qui permet d'obtenir le serveur qui permet de se connecter à la base de donnée
        /// </summary>
        public static string Server
        {
            get { return server; }
        }

        /// <summary>
        /// Propriété (set) qui permet d'obtenir le nom de la base de donnée où se trouve les tables sql
        /// </summary>
        public static string Database
        {
            get { return database; }
        }

        /// <summary>
        /// Fonction qui permet de retourner toutes les informations de connexion à la base
        /// </summary>
        /// <returns>Une chaine de caractère</returns>
        public static string GetStringConnection()
        {
            return string.Format("user={0};password={1};server={2};database={3}", user, password, server, database);
        }

        /// <summary>
        /// Méthode qui permet d'instancier une chaine de connexion
        /// </summary>
        /// <param name="user">Utilisateur de la base</param>
        /// <param name="password">Mot de passe de la base</param>
        /// <param name="server">Nom du serveur de la base</param>
        /// <param name="database">Nom de la base de donnée</param>
        public static void SetStringConnection(string user, string password, string server, string database)
        {
            DaoConnection.user = user;
            DaoConnection.password = password;
            DaoConnection.server = server;
            DaoConnection.database = database;
        }

        /// <summary>
        /// Fonction qui permet de se connecter à la base de donnée
        /// </summary>
        /// <returns>Une connexion de type MySqlConnection</returns>
        public static MySqlConnection GetMySqlConnection()
        {
            if (DaoConnection.mySqlConnection == null)
            {
                DaoConnection.mySqlConnection = new MySqlConnection(DaoConnection.GetStringConnection());
            }
            return DaoConnection.mySqlConnection;
        }
    }
}
