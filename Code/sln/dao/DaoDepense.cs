﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Coloc.Model;

namespace Coloc.Dao
{
    public class DaoDepense
    {
        /// <summary>
        /// Fonction qui permet d'obtenir la liste des dépenses non reparties
        /// </summary>
        /// <returns>Une liste de dépense</returns>
        public List<Depense> GetAllNonReparti()
        {
            List<Depense> listeDepense = new List<Depense>();
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select id,dateDepense,text,justificatif,montant,reparti,idColocataire from depense where reparti = 0", cnx);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int id = rdr.GetInt32("id");
                DateTime dateDepense = rdr.GetDateTime("dateDepense");
                decimal montant = rdr.GetDecimal("montant");
                string justificatif = rdr.GetString("justificatif");
                string text = rdr.GetString("text");
                bool reparti = rdr.GetBoolean("reparti");
                int idColoc = rdr.GetInt32("idColocataire");
                listeDepense.Add(new Depense(id,dateDepense,text,justificatif,montant,reparti,idColoc));
            }
            cnx.Close();
            return listeDepense;
        }

        /// <summary>
        /// Fonction qui permet d'obtenir la liste des dépenses reparties
        /// </summary>
        /// <returns>Une liste de dépense</returns>
        public List<Depense> GetAllReparti()
        {
            List<Depense> listeDepense = new List<Depense>();
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select id,dateDepense,text,justificatif,montant,reparti,idColocataire from depense where reparti = 1", cnx);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int id = rdr.GetInt32("id");
                DateTime dateDepense = rdr.GetDateTime("dateDepense");
                decimal montant = rdr.GetDecimal("montant");
                string justificatif = rdr.GetString("justificatif");
                string text = rdr.GetString("text");
                bool reparti = rdr.GetBoolean("reparti");
                int idColoc = rdr.GetInt32("idColocataire");
                listeDepense.Add(new Depense(id, dateDepense, text, justificatif, montant, reparti, idColoc));
            }
            cnx.Close();
            return listeDepense;
        }

        /// <summary>
        /// Fonction qui permet d'obtenir toutes les dépenses d'un colocataire
        /// </summary>
        /// <param name="idColoc">Id du colocataire</param>
        /// <returns>Une liste de dépense</returns>
        public List<Depense> GetAllDepense(int idColoc)
        {
            List<Depense> listeDepense = new List<Depense>();
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("select id,dateDepense,text,justificatif,montant,reparti,idColocataire from depense where idColocataire=@id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = idColoc;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int id = rdr.GetInt32("id");
                DateTime dateDepense = rdr.GetDateTime("dateDepense");
                decimal montant = rdr.GetDecimal("montant");
                string justificatif = rdr.GetString("justificatif");
                string text = rdr.GetString("text");
                bool reparti = rdr.GetBoolean("reparti");
                int idColocataire = rdr.GetInt32("idColocataire");
                listeDepense.Add(new Depense(id, dateDepense, text, justificatif, montant, reparti, idColocataire));
            }
            cnx.Close();
            return listeDepense;
        }

        /// <summary>
        /// Méthode qui permet d'insérer une Dépense dans la base de donnée
        /// </summary>
        /// <param name="uneDepense">Objet de type Depense qui correspond à une dépense</param>
        public void Insert(Depense uneDepense)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("insert into depense(dateDepense, text, justificatif, montant, reparti, idColocataire) values(@dateDepense,@text,@justificatif,@montant,@reparti,@idColocataire)", cnx);
            cmd.Parameters.Add(new MySqlParameter("@dateDepense", MySqlDbType.DateTime));
            cmd.Parameters.Add(new MySqlParameter("@text", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@justificatif", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@montant", MySqlDbType.Decimal));
            cmd.Parameters.Add(new MySqlParameter("@idColocataire", MySqlDbType.VarChar));
            cmd.Parameters.Add(new MySqlParameter("@reparti", MySqlDbType.VarChar));

            cmd.Parameters["@dateDepense"].Value = uneDepense.DateDepense;
            cmd.Parameters["@text"].Value = uneDepense.Text;
            cmd.Parameters["@justificatif"].Value = uneDepense.Justificatif;
            cmd.Parameters["@montant"].Value = uneDepense.Montant;
            cmd.Parameters["@idColocataire"].Value = uneDepense.IdColoc;
            bool reparti = uneDepense.Reparti;
            switch (reparti){
                case true:
                    cmd.Parameters["@reparti"].Value = 1;
                    break;
                case false:
                    cmd.Parameters["@reparti"].Value = 0;
                    break;
                default:
                    break;
            }
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        /// <summary>
        /// Méthode qui permet de supprimer une Dépense de la base de donnée
        /// </summary>
        /// <param name="uneDepense">Objet de type Depense qui correspond à une dépense</param>
        public void Delete(Depense uneDepense)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("delete from depense where id = @id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = uneDepense.IdDepense;
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        /// <summary>
        /// Méthode qui permet de supprimer toutes les dépenses du colocataire dans la base donnée
        /// </summary>
        /// <param name="idColoc">Objet de type Depense qui correspond à une dépense</param>
        public void Delete(int idColoc)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("delete from depense where idColocataire = @id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = idColoc;
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        /// <summary>
        /// Méthode qui permet de modifier une dépense dans la base de donnée
        /// </summary>
        /// <param name="uneDepense">Objet de type Depense qui correspond à une dépense</param>
        public void Update(Depense uneDepense)
        {
            MySqlConnection cnx = DaoConnection.GetMySqlConnection();
            cnx.Open();
            MySqlCommand cmd = new MySqlCommand("update depense set dateDepense = @date, text = @text, justificatif = @justificatif, montant = @montant, reparti = @reparti, idColocataire = @idColoc where id = @id", cnx);
            cmd.Parameters.Add(new MySqlParameter("@id", MySqlDbType.Int32));
            cmd.Parameters["@id"].Value = uneDepense.IdDepense;
            cmd.Parameters.Add(new MySqlParameter("@idColoc", MySqlDbType.Int32));
            cmd.Parameters["@idColoc"].Value = uneDepense.IdColoc;
            cmd.Parameters.Add(new MySqlParameter("@date", MySqlDbType.DateTime));
            cmd.Parameters["@date"].Value = uneDepense.DateDepense;
            cmd.Parameters.Add(new MySqlParameter("@text", MySqlDbType.VarChar));
            cmd.Parameters["@text"].Value = uneDepense.Text;
            cmd.Parameters.Add(new MySqlParameter("@justificatif", MySqlDbType.VarChar));
            cmd.Parameters["@justificatif"].Value = uneDepense.Justificatif;
            cmd.Parameters.Add(new MySqlParameter("@montant", MySqlDbType.Decimal));
            cmd.Parameters["@montant"].Value = uneDepense.Montant;
            cmd.Parameters.Add(new MySqlParameter("@reparti", MySqlDbType.Bit));
            if(uneDepense.Reparti == true)
            {
                cmd.Parameters["@reparti"].Value = 1;
            }
            else
            {
                cmd.Parameters["@reparti"].Value = 0;
            }
            
            cmd.ExecuteNonQuery();
            cnx.Close();
        }
    }


}

