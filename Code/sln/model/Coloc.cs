﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Coloc
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private string telephone;
        private List<Depense> lesDepenses;

        public Coloc(int id, string nom, string prenom, string mail, string telephone)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.telephone = telephone;
            this.lesDepenses = new List<Depense>();
        }

        public int IdColoc
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public string Nom
        {
            get { return this.nom; }
            set { this.nom = value; }
        }
        public string Prenom
        {
            get { return this.prenom; }
            set { this.prenom = value; }
        }
        public string Mail
        {
            get { return this.mail; }
            set { this.mail = value; }
        }
        public string Telephone
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }
        
        public int Count
        {
            get { return this.lesDepenses.Count; }
        }
        public void Add(Depense uneDepense)
        {
            this.lesDepenses.Add(uneDepense);
        }
        public void Remove(Depense uneDepense)
        {
            this.lesDepenses.Remove(uneDepense);
        }
        public List<Depense> GetListeDepense()
        {
            return this.lesDepenses;
        }

        public decimal APaye()
        {
            decimal depenses = 0.0m;
            foreach(Depense uneDepense in this.lesDepenses)
            {
                depenses += uneDepense.Montant;
            }
            return depenses;
        }

        public override string ToString()
        {
            return string.Format("Colocataire : {0} {1}, Mail : {2}, Téléphone : {3}",this.nom,this.prenom,this.mail,this.telephone);
        }



    }
}
