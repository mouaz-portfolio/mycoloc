﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coloc.Model
{
    public class Colocataires
    {
        private List<Colocataire> lesColocs;

        /// <summary>
        /// Constructeur de la classe qui Instancie et créer une colection de colocataire
        /// </summary>
        public Colocataires()
        {
            this.lesColocs = new List<Colocataire>();
        }

        /// <summary>
        /// Propriete (get) qui permet de retourner la longueur de la collection des colocataires
        /// </summary>
        public int Count
        {
            get { return this.lesColocs.Count; }
        }

        /// <summary>
        /// Méthode qui permet d'ajouter un colocataire à la liste des colocataires
        /// </summary>
        /// <param name="unColoc">Objet de type Colocataire qui correspond à un colocataire</param>

        public void Add(Colocataire unColoc)
        {
            this.lesColocs.Add(unColoc);
        }

        /// <summary>
        /// Méthode qui permet de supprimer un colocataire de la liste des colocataires
        /// </summary>
        /// <param name="unColoc">Objet de type Colocataire qui correspond à un colocataire</param>
        public void Remove(Colocataire unColoc)
        {
            this.lesColocs.Remove(unColoc);
        }

        /// <summary>
        /// Fonction qui permet d'obtenir la liste des colocataires
        /// </summary>
        /// <returns>Retourne une collection de colocataire</returns>
        public List<Colocataire> GetListeColoc()
        {
            return this.lesColocs;
        }
    }
}
